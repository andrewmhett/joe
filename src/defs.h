#ifndef DEFS_H
#define DEFS_H

#define FRAMERATE 1.5 // Frames per second
#define NUM_FRAMES 10

#define FRAME_HEIGHT 15
#define FRAME_WIDTH 32
#define FOAM_HEIGHT 2
#define ROW_WIDTHS {18,20,22,22,22,22,22,22,22,22}
#define ROW_STARTS {3,2,1,1,1,1,1,1,1,1}

#define CUP_ROWS {\
"       \\    \\   /",\
"        \\   /    \\",\
"        /   |     /",\
"+----------------------+",\
"|                      |",\
"|                      |-----*",\
"|                      |      \\",\
"|                      |       |",\
"|                      |       |",\
"|                      |       |",\
"|                      |      /",\
"|                      |-----*",\
" \\                    /",\
"  \\                  /",\
"   *----------------*"\
}

void brew_coffee(WINDOW*);

#endif
