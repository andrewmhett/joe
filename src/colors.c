#include <ncurses.h>

#include "colors.h"
#include "defs.h"

void init_colors(void) {
    if (!has_colors()) return;

    start_color();

    init_color(COLOR_MAGENTA, FOAM_RGB);
    init_color(COLOR_BLUE, LIQUID_RGB);

    init_pair(1, COLOR_MAGENTA, COLOR_MAGENTA);
    init_pair(2, COLOR_BLUE, COLOR_BLUE);
}