#ifndef COLORS_H
#define COLORS_H

#define FOAM_RGB 255, 255, 255
#define LIQUID_RGB 180, 151, 135

void init_colors(void);
void reset_colors(void);

#endif