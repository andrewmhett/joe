#include <ncurses.h>

#include "defs.h"
#include "colors.h"

void brew_coffee(WINDOW* window) {
    int row_starts[] = ROW_STARTS;
    int row_widths[] = ROW_WIDTHS;
    char* cup_rows[] = CUP_ROWS;

    for (int frame = 0; frame < NUM_FRAMES; frame++) {
        clear();

        int offset_x = (getmaxx(window) / 2) - (FRAME_WIDTH / 2);
        int offset_y = (getmaxy(window) / 2) - (FRAME_HEIGHT / 2);

        for (int row = 0; row < FRAME_HEIGHT; row++) {
            mvprintw(offset_y + row, offset_x, "%s", cup_rows[row]);
        }

        if (has_colors()) {
            attron(COLOR_PAIR(2));

            for (int filled_row = 0; filled_row < frame - FOAM_HEIGHT + 1; filled_row++) {
                int y = FRAME_HEIGHT - filled_row + offset_y - 2;
                int x = row_starts[filled_row] + offset_x;
                mvhline(y, x, ' ', row_widths[filled_row]);
            }

            attron(COLOR_PAIR(1));

            for (int filled_row = frame - FOAM_HEIGHT + 1; filled_row <= frame; filled_row++) {
                if (filled_row < 0) continue;
                int y = FRAME_HEIGHT - filled_row + offset_y - 2;
                int x = row_starts[filled_row] + offset_x;
                mvhline(y, x, ' ', row_widths[filled_row]);
            }
        }

        if (has_colors()) attroff(COLOR_PAIR(2));

        refresh();

        napms(1000 / FRAMERATE);
    }
}

int main(void) {
    WINDOW * window = initscr();

    noecho();
    curs_set(FALSE);

    init_colors();

    brew_coffee(window);

    endwin();

    reset_shell_mode();
    curs_set(TRUE);

    return 0;
}